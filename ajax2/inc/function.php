<?php

function debug($tableau)
{
    echo '<pre style="height:100px;overflow-y: scroll;font-size:.5rem;padding: .6rem; font-family: Consolas, Monospace;background-color: #000;color:#fff;">';
    print_r($tableau);
    echo '</pre>';
}

function validText($er, $data, $key, $min, $max)
{
    if(!empty($data)) {
        if(mb_strlen($data) < $min) {
            $er[$key] = 'min '.$min.' caractères';
        } elseif(mb_strlen($data) >= $max) {
            $er[$key] = 'max '.$max.' caractères';
        }
    } else{
        $er[$key] = 'Veuillez renseigner ce champ';
    }
    return $er;
};
function validEmail($er, $data, $key)
{
    if(!empty($data)) {
        if (!filter_var($data, FILTER_VALIDATE_EMAIL)){
            $er[$key] = 'Veuillez renseigner un email valide';  
        }  
    } else{
        $er[$key] = 'Veuillez renseigner ce champ';
    }
    return $er;
}

function clearxxs ($key){
    return trim(strip_tags($_POST[$key]));
}

// a mettre ds la bao
function getBeer($id ){
    global $pdo;
    $query = $pdo->prepare("SELECT*FROM beer WHERE id= :id");
    $query ->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $beer = $query->fetch();  
    return $beer; 
}
// fonction pour afficher un certain nombre d'article par date ascendante ou descendante
function getAllBeer($limit , $order= 'DESC'){
    global $pdo;
    $query = $pdo->prepare("SELECT*FROM beer ORDER BY created_at $order LIMIT $limit");    
    $query->execute();
    $beers = $query->fetchAll();  
    return $beers; 
}

function dateSite($date, $format = 'd/m/Y'){
    return date($format, strtotime($date));    
}

// function generateGroupeName ($tab1, $tab2){
//     if(!empty($tab1 && $tab2)){
// $tab = array('tab1');
// $tab += array('tab2');
// $combianaison = array_merge($tab1, $tab2);
// debug($combianaison);
//     }
// }
// $defaults = array('avare','paresseux','cruel','distrait', 'médiocre','gourmand', 'infidèle','capricieux', 'colérique', 'ténieux');
// // creation tableau plat
// $plats =array('cuisses de grenouille', 'tartiflette','blanquette', 'quiche lorraine', 'crème brulée', 'lentilles', 'raclette','mousse choc', 'bourguignon', 'gratin de chicon');

// // Créer une fonction "generationGroupeName" permettant d'afficher une combinaison d'un plat avec un defaut au hasard
// // "tartiflette frustree"
// // afficher dans une div, text devra être rose, tous les mots devront commencer par une majuscule

// // création d'une fonction qui prend 2 paramètres (les deux tableaux)


