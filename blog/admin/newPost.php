<?php
// importer les fonctions et le lien avec la bdd
require('../inc/fonction.php');
require('../inc/pdo.php');
$title = "NewPost";
// Traitement PHP
// Formulaire est soumis ???
$success = false;
// création d'un tableau d'erreur
$errors = array();
// vérifier si il y a un post submit
if (!empty($_POST['submitted'])) {
    // Faille XSS trim pour enlever les espaces et strip_tags pour enlever les balises pour éviter l'injection de code
    $title = trim(strip_tags($_POST['title']));
    $content = trim(strip_tags($_POST['content']));
    $author = trim(strip_tags($_POST['author']));
    $status = trim(strip_tags($_POST['status']));
    // Validation
    $errors = validText($errors, $title, 'title', 2, 30);
    $errors = validText($errors, $content, 'content', 2, 100);
    $errors = validText($errors, $author, 'author', 2, 30);
    $errors = validText($errors, $status, 'status', 2, 30);


    if (count($errors) === 0) {
        // insertion en BDD si aucune error en envoyant à la dbb une requete
        $sql = "INSERT INTO articles ( title, content, author, created_at, modified_at, status)  VALUES ( :title, :content, :author, NOW(), NOW(), :status );";
        // INJECTION SQL
        // prépare une requête à l'exécution et retourne un objet
        $query = $pdo->prepare($sql);
        // associe une valeur à un parametre
        $query->bindValue(':title', $title, PDO::PARAM_STR);
        $query->bindValue(':content', $content, PDO::PARAM_STR);
        $query->bindValue(':author', $author, PDO::PARAM_STR);
        $query->bindValue(':status', $status, PDO::PARAM_STR);
        //  execute la requete
        $query->execute();
        // Retourne l'identifiant de la dernière ligne insérée ou la valeur d'une séquence
        $last_id = $pdo->lastInsertId();
        // nvoie sur une autre page une fois la requete exécutée
        header('Location:listPost.php?id=' . $last_id);
    }
    $success = true;
}
?>
<!-- insertion du header (option) -->
<?php include('inc/header-back.php'); ?>
<!-- création d'un formulaire html -->
<h1>Ajouter un article &#128526;</h1>
<form action="" method="post" novalidate>
    <label for="title">Titre</label>
    <input type="text" name="title" required id="title" value="<?php if (!empty($_POST['title'])) {
                                                                    echo $_POST['title'];
                                                                } ?>">
    <span class="error"><?php if (!empty($errors['title'])) {
                            echo $errors['title'];
                        } ?></span>

    <label for="content">Contenu</label>
    <textarea name="content" id="content" cols="30" rows="10"><?php if (!empty($_POST['content'])) {
                                                                    echo $_POST['content'];
                                                                } ?></textarea>
    <span class="error"><?php if (!empty($errors['content'])) {
                            echo $errors['content'];
                        } ?></span>

    <label for="author">Auteur</label>
    <input type="author" name="author" required id="author" value="<?php if (!empty($_POST['author'])) {
                                                                        echo $_POST['author'];
                                                                    } ?>">
    <span class="error"><?php if (!empty($errors['author'])) {
                            echo $errors['author'];
                        } ?></span>

    <!-- <label for="status">Status</label>
        <input type="status" name="status" required id="status" value="<?php if (!empty($_POST['status'])) {
                                                                            echo $_POST['status'];
                                                                        } ?>">
        <span class="error"><?php if (!empty($errors['status'])) {
                                echo $errors['status'];
                            } ?></span> -->
    <?php
    // pour offrir deux option à status création d'un tableau avec les 2 choix
    $status = array(
        'draft' => 'brouillon',
        'publish' => 'Publié'
    );

    ?>
    <select name="status">
        <option value="">---------------------</option>
        <!-- faire une fonction  -->
        <?php foreach ($status as $key => $value) {
            $selected = '';
            if (!empty($_POST['status'])) {
                if ($_POST['status'] == $key) {
                    $selected = ' selected="selected"';
                }
            }
        ?>
            <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
        <?php } ?>
    </select>
    <span class="error"><?php if (!empty($errors['status'])) {
                            echo $errors['status'];
                        } ?></span>




    <input type="submit" name="submitted" value="Ajouter cet article &#128523;">
</form>