<?php
// chercher des liens dans le include
require('../inc/fonction.php');
require('../inc/pdo.php');
include('./inc/header-back.php');

?>
<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $sql_edit_article = "SELECT * FROM articles WHERE id = :id";
    $query = $pdo->prepare($sql_edit_article);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $article = $query->fetch();
}

// En cas d'erreur retourne un tableau
$errors = [];
if (!empty($_POST['submitted'])) {

    // Faille XSS enlève les espace avec trim et les balises avec strip_tags pour eviter l'injection de code
    $title = trim(strip_tags($_POST['title']));
    $content = trim(strip_tags($_POST['content']));
    $author = trim(strip_tags($_POST['author']));
    $status = trim(strip_tags($_POST['status']));
    // Validation
    $errors = validText($errors, $title, 'title', 2, 100);
    $errors = validText($errors, $content, 'content', 2, 100);
    $errors = validText($errors, $author, 'author', 2, 100);
    $errors = validText($errors, $status, 'status', 2, 100);
}

// Ajouter un commentaire
$sql = "SELECT * FROM comments WHERE id_article = :id";
$query = $pdo->prepare($sql);
$query->bindValue(':id', $id, PDO::PARAM_INT);
$query->execute();
$comments = $query->fetchAll();



$errors = [];
if (!empty($_POST['submitted'])) {
    $author = trim(strip_tags($_POST['author']));
    $content = trim(strip_tags($_POST['content']));
    $errors = validText($errors, $content, 'content', 2, 100);
    $errors = validText($errors, $author, 'author', 2, 100);
    if (count($errors) == 0) {
        $sql = "INSERT INTO comments (id_article,content, author, created_at,modified_at,status)
            VALUES (:idarticle,:content,:author,NOW(),NOW(),'new')";
        $query = $pdo->prepare($sql);
        $query->bindValue(':author', $author, PDO::PARAM_STR);
        $query->bindValue(':content', $content, PDO::PARAM_STR);
        $query->bindValue(':idarticle', $id, PDO::PARAM_INT);
        $query->execute();
        header('Location: single.php?id=' . $id);
        die();
    }
}

?>
<!-- on edit par exemple l'utilisateur pour poouvoir proceder à la modification -->
<h1>Article only</h1>
<form action="" method="post" novalidate>
    <label for="title">
        <span>Titre:</span>
        <input type="text" name="title" value="<?= $article['title'] ?>">
        <span class="error"><?php if (!empty($errors['title'])) {
                                echo $errors['title'];
                            } ?></span>

    </label>
    <label for="content">
        <span>Contenu :</span>
        <textarea name="content" id="content" cols="30" rows="10"><?= $article['content'] ?></textarea>
        <span class="error"><?php if (!empty($errors['content'])) {
                                echo $errors['content'];
                            } ?></span>
    </label>
    <label for="author">
        <span>Auteur :</span>
        <input type="text" name="author" value="<?= $article['author'] ?>">
        <span class="error"><?php if (!empty($errors['author'])) {
                                echo $errors['author'];
                            } ?></span>
    </label>
    <label for="status">
        <span>Statut :</span>
        <input type="text" name="status" value="<?= $article['status'] ?>">
        <span class="error"><?php if (!empty($errors['status'])) {
                                echo $errors['status'];
                            } ?></span>
    </label>

</form>
<!-- Ajouter un commentaire création d'un formulaire commentaire -->
<h2>Ajouter un commentaire</h2>
<form action="" method="post" class="wrap2">
    <label for="author">Auteur </label>
    <input type="text" name="author" value="<?= $article['author'] ?>">
    <span class="error"><?php if (!empty($errors['author'])) {
                            echo $errors['author'];
                        } ?></span>

    <label for="content">Commentaire </label>
    <textarea name="content" id="content2" cols="30" rows="10"><?= $article['content']; ?></textarea>
    <span class="error"><?php if (!empty($errors['content'])) {
                            echo $errors['content'];
                        } ?></span>

    <input type="submit" name="submitted" value="Ajouter">
</form>
<!-- S'il n'y a pas d'absence de commentaire alors -->
<?php if (!empty($comments)) { ?>
    <h2>Les commentaire</h2>
    <?php foreach ($comments as $comment) { ?>
        <div class="comment">
            <p>Auteur : <?= $comment['author']; ?></p>
            <p><?= $comment['content']; ?></p>
            <hr>
        </div>
    <?php } ?>
<?php } ?>
</div>