<?php
// importation des fonction et de la connection à la base de données
require('../inc/pdo.php');

include('./inc/header-back.php');

$title = "listPost";
?>
<?php
// requete pour selectionner tous les éléments de la bdd et les ordonner par date de création
$select_articles = "SELECT * FROM articles ORDER BY title DESC";
// prepare la requete à l'éxecution et repour un objet
$query = $pdo->prepare($select_articles);
// execute la requete
$query->execute();
// retourne tous les éléments et les affiche
$articles = $query->fetchAll();
?>
<!-- tableau affichant la réponse en html -->
<h1>Liste des articles</h1>
<table>
    <thead>
        <tr class="listTab">
            <!-- <th class="listcolum">id</th> -->
            <th class="listcolum">title</th>
            <th class="listcolum">content</th>
            <th class="listcolum">auteur</th>
            <th class="listcolum">status</th>

        </tr>
    </thead>
    <tbody>
        <!-- pour chaque reponse afficher les paramètres demandé ici id, nom, prenom ... -->
        <?php foreach ($articles as $article) { ?>
            <tr>
                <!-- <td class="listrow"><?= $article['id'] ?></td> -->
                <td class="listrow"><?= $article['title'] ?></td>
                <td class="listrow"><?= $article['content'] ?></td>
                <td class="listrow"><?= $article['author'] ?></td>
                <td class="listrow"><?= $article['status'] ?></td>
                <td class="listrow"><a href="single.php?id=<?= $article['id'] ?>">Editer</a></td>
                <td class="listrow"><a href="updateArt.php?id=<?= $article['id'] ?>">Modifier</a></td>
                <td class="listrow"><a href="deleteArt.php?id=<?= $article['id'] ?>">Supprimer</a></td>
            </tr>

        <?php } ?>
    </tbody>
</table>
<button>
    <a href="./newPost.php">Ajouter un article</a>
</button>