<?php
require('../inc/pdo.php');
require('../inc/fonction.php');

include('./inc/header-back.php');


?>
<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $articles = "SELECT * FROM articles WHERE id = :id";
    $query = $pdo->prepare($articles);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $article = $query->fetch();
    // debug($article);

    $errors = [];
    if (!empty($_POST['submitted'])) {

        // Faille XSS
        $title = trim(strip_tags($_POST['title']));
        $content = trim(strip_tags($_POST['content']));
        $author = trim(strip_tags($_POST['author']));
        $status = trim(strip_tags($_POST['status']));
        // Validation
        $errors = validText($errors, $title, 'title', 2, 100);
        $errors = validText($errors, $content, 'content', 2, 100);
        $errors = validTExt($errors, $author, 'author', 2, 100);
        $errors = validTExt($errors, $status, 'status', 2, 100);


        if (count($errors) === 0) {
            $requete_update = "UPDATE articles SET title= :title, content= :content, author = :author, modified_at=NOW(), status = :status  WHERE id= :id";
            $query = $pdo->prepare($requete_update);
            $query->bindValue(':title', $title, PDO::PARAM_STR);
            $query->bindValue(':content', $content, PDO::PARAM_STR);
            $query->bindValue(':author', $author, PDO::PARAM_STR);
            $query->bindValue(':status', $status, PDO::PARAM_STR);
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            header('Location: listPost.php');
        }
    }
?>
    <!-- on edit par exemple l'utilisateur pour poouvoir proceder à la modification -->
    <h1>Modifier l'article</h1>
    <form action="" method="post" novalidate>
        <label for="title">
            <span>Titre:</span>
            <input type="text" name="title" value="<?= $article['title'] ?>">
            <span class="error"><?php if (!empty($errors['title'])) {
                                    echo $errors['title'];
                                } ?></span>

        </label>
        <label for="content">
            <span>Contenu :</span>
            <textarea name="content" id="content" cols="30" rows="10"><?= $article['content'] ?></textarea>
            <span class="error"><?php if (!empty($errors['content'])) {
                                    echo $errors['content'];
                                } ?></span>
        </label>
        <label for="author">
            <span>Auteur :</span>
            <input type="text" name="author" value="<?= $article['author'] ?>">
            <span class="error"><?php if (!empty($errors['author'])) {
                                    echo $errors['author'];
                                } ?></span>
        </label>
        <!-- <label for="status">
            <span>Statut :</span>
            <input type="text" name="status" value="<?= $article['status'] ?>">
            <span class="error"><?php if (!empty($errors['status'])) {
                                    echo $errors['status'];
                                } ?></span>
        </label> -->
        <?php
        // pour offrir deux option à status création d'un tableau avec les 2 choix
        $status = array(
            'draft' => 'brouillon',
            'publish' => 'Publié'
        );

        ?>
        <select name="status">
            <option value="">---------------------</option>
            <!-- faire une fonction  -->
            <?php foreach ($status as $key => $value) {
                $selected = '';
                if (!empty($_POST['status'])) {
                    if ($_POST['status'] == $key) {
                        $selected = ' selected="selected"';
                    }
                }
            ?>
                <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
            <?php } ?>
        </select>
        <span class="error"><?php if (!empty($errors['status'])) {
                                echo $errors['status'];
                            } ?></span>


        <input type="submit" name="submitted" value="modifier">
    </form>

<?php } ?>