<?php


function debug($tableau)
{
    echo '<pre style="height:100px;overflow-y: scroll;font-size:.5rem;padding: .6rem; font-family: Consolas, Monospace;background-color: #000;color:#fff;">';
    print_r($tableau);
    echo '</pre>';
}
function validText($er, $data, $key, $min, $max)
{
    if(!empty($data)) {
        if(mb_strlen($data) < $min) {
            $er[$key] = 'min '.$min.' caractères';
        } elseif(mb_strlen($data) >= $max) {
            $er[$key] = 'max '.$max.' caractères';
        }
    } else{
        $er[$key] = 'Veuillez renseigner ce champ';
    }
    return $er;
};

// supprimer un article

function supp(){
    global $pdo;
// vérification de l'ID dans la bdd
if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
   $id = $_GET['id'];
    // requete bdd
   $sql = "SELECT * FROM blog WHERE id = $id";
     // on prepare une requête à l'exécution et retourne un objet
   $query = $pdo->prepare($sql);
      //  on associe une valeur à un paramètre
   $query ->bindValue(':id', $id, PDO::PARAM_INT);
       // exécution de la requete
   $query->execute();
     // une fois la requete executé on retourne sur une autre page
   $article = $query->fetch();

//  verification si l'id dans l'URL existe. s'il existe on fait une requete à la bdd delete
if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    // requete bdd
    $sql_supp = "DELETE  FROM articles WHERE id = :id";
    // on prepare une requête à l'exécution et retourne un objet
    $query = $pdo->prepare($sql_supp);
    //  on associe une valeur à un paramètre
    $query->bindValue(':id',$id, PDO::PARAM_INT);
    // exécution de la requete
    $query->execute();
    // une fois la requete executé on retourne sur une autre page
    header('Location: index.php');
    }
   }
 else {
    // si erreur on arrete le code ab=vec message d'erreur
    //die('404');
   die("requete impossible. il y a une erreur");
}
}

// création d'une fonction qui prend 3 paramètre(page, nb d'élément dans la page et la somme des éléments)
function pagination($page,$itemPerpage,$count)
{
    // on crée un liste en html
    $html = '';
    $html .= '<ul class="paginate">';
    // pour revenir sur les pages précedentes
    if($page > 1) {
        $paged = $page - 1;
        $html .= '<li><a href="index.php?page='.$paged.'">Précédent</a></li>';
    }
    // pour avancer dans les pages
    if($page * $itemPerpage < $count) {
        $paged = $page + 1;
        $html .= '<li><a href="index.php?page='.$paged.'">Suivant</a></li>';
    }
    $html .= '</ul>';
    return $html;
}
function countArticle()
{
    // on appelle la bdd
    global $pdo;
    // requete bdd
    $sql = "SELECT COUNT(id) FROM articles";
    // on prépare la requête
    $query = $pdo->prepare($sql);
     // exécution de la requete
    $query->execute();
    // On utilise fetchColumn car ca retourne une somme
    return $query->fetchColumn();
}


 