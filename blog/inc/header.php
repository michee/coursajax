<!doctype html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="asset/css/style.css">
    <title>Front Office</title>
</head>

<body>
    <header>

        <div class="logo"><img src="https://svgsilh.com/svg_v2/606684.svg" alt="logo blog écris au pinceau et en noir">
        </div>
        <div class="presentation_header">

            <nav>
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="admin/index.php">Admin</a></li>
                    <li><a href="admin/listPost.php">Liste des Posts</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <div id="content">