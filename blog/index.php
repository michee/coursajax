<?php

require('inc/pdo.php');
require('inc/fonction.php');
require('inc/request.php');


$page = 1;
$itemPerpage = 5;
$offset = 0;
if (!empty($_GET['page']) && ctype_digit($_GET['page'])) {
    $page = $_GET['page'];
    $offset = ($page - 1) * $itemPerpage;
}

$sql = "SELECT * FROM articles WHERE status = 'publish' ORDER BY created_at DESC LIMIT $itemPerpage OFFSET $offset";
$query = $pdo->prepare($sql);
$query->execute();
$articles = $query->fetchAll();

$count = countArticle();

include('inc/header.php'); ?>
<div class="wrap">
    <h1>Blog</h1>
    <?= pagination($page, $itemPerpage, $count); ?>
    <section id="articles">
        <?php foreach ($articles as $article) { ?>
            <div class="article">
                <h2><?= $article['title']; ?></h2>
                <a href="../blog/admin/single.php ?id=<?= $article['id']; ?>">
                    <img src="https://picsum.photos/id/<?= $article['id'] + 45; ?>/300/200" alt="">
                </a>
            </div>
        <?php } ?>
    </section>
    <?= pagination($page, $itemPerpage, $count); ?>
</div>
<?php include('inc/footer.php');
