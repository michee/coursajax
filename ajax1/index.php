<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AJAX 1 - </title>
</head>

<body>

    <?php
    //$dede = 'dede';
    //echo '<pre>';
    //print_r($GLOBALS);
    //echo '</pre>';
    ?>
    <div id="btn1">Ajouter du texte ci-dessous 1</div>
    <div id="response1"></div>

    <div id="btn2">Ajouter du texte ci-dessous 2</div>
    <div id="response2"></div>

    <div id="btn3">Ajouter du texte ci-dessous 3</div>
    <div id="response3"></div>

    <div id="btn4">Ajouter du texte ci-dessous 4</div>
    <div id="response4"></div>


    <script>
        // Test 1
        const btn1 = document.querySelector('#btn1');
        const response1 = document.querySelector('#response1');

        function getTexte1(event) {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    //console.log(this.responseText);
                    response1.innerHTML = this.responseText
                }
            }
            xhttp.open('GET', 'demo1.php');
            xhttp.send();
        }
        btn1.addEventListener('click', getTexte1);
        //////////////////
        // Test 2
        /////////////////
        const btn2 = document.querySelector('#btn2');
        const response2 = document.querySelector('#response2');

        function getUser() {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    //console.log(this.responseText);
                    response2.innerHTML = this.responseText
                }
            }
            xhttp.open('GET', 'ajax/demo2.php?nom=quidel&prenom=antoine&age=44');
            xhttp.send();
        }
        btn2.addEventListener('click', getUser);
        //////////////////
        // Test 3
        /////////////////
        const btn3 = document.querySelector('#btn3');
        const response3 = document.querySelector('#response3');

        function postFruits() {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    console.log(this.responseText);
                }
            }
            xhttp.open('POST', 'ajax/demo2.php', true);
            xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
            xhttp.send("fruit=banane&music=basse");
        }
        btn3.addEventListener('click', postFruits);

        //////////////////
        // Test 4
        /////////////////
        const btn4 = document.querySelector('#btn4');
        const response4 = document.querySelector('#response4');

        function getDog() {
            fetch('https://dog.ceo/api/breeds/image/random')
                .then(function(response) {
                    return response.json()
                })
                .then(function(data) {
                    const img = (data.message)
                    const image = document.createElement('img')
                    image.src = img
                    const container = document.querySelector("#response4")
                    container.innerHTML = ' ';
                    container.append(image)
                })
                .catch(function(err) {
                    console.log((err));
                })
        }
        btn4.addEventListener('click', getDog);
    </script>

</body>

</html>